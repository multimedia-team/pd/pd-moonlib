Source: pd-moonlib
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-puredata,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/MetaluNet/moonlib/
Vcs-Git: https://salsa.debian.org/multimedia-team/pd/pd-moonlib.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/pd/pd-moonlib

Package: pd-moonlib
Architecture: any
Depends:
 puredata-core (>= 0.53~),
 ${misc:Depends},
 ${puredata:Depends},
 ${shlibs:Depends},
Recommends:
 ${puredata:Recommends},
Suggests:
 ${puredata:Suggests},
Provides:
 ${puredata:Provides},
Description: library of Pd objects related to GUI control
 Moonlib includes three sub-sections: nilib, which is a kind of wrapper
 between Pd and Gtk, sublib, which is a collection of gui control objects, and
 other, which are miscellaneous objects:
 .
  * tabenv: like env~, an enveloppe follower, but computing on a table,
            so possibly much speeder than real-time env~'s computation
  * tabsort, tabsort2: returns the indices of the sorted table,
                       tabsort2 is bidimensional
  * gamme: one octave of a piano keyboard used to filter/choose
           notes in a selected scale
  * absolutepath/relativepath: to use files (sounds, texts, presets,
                               images, programs...) nested in the patch's
                               directory (and in subdirs)
  * sarray and slist: to creates shared dynamic arrays or lists with symbols
  * sfread2~ and readsfv~: to pitch the direct-from-disk reading of sound files
  * dinlet~: an inlet~ with a default value (when nothing is connected to it)
  * mknob: a round knob ala iemgui vslider (with its "properties" window)
  * dispatch: creates one bus name for many buttons' buses: from the
              N pairs (slider1-snd/slider1-rcv) ... (sliderN-snd/sliderN-rcv),
              creates only one pair of buses named (slider-snd/slider-rcv),
              in which datas are prepended by the number of the "sub-bus"
  * joystik: an improvement of Joseph A. Sarlo's joystick
  * image: an improvement  of Guenter Geiger's one. Same name, but it's
           compatible. Here you can share images through different objects,
           preload a list of images, and animate this list.
